import stanford.karel.*;

// Goal: Build a Pyramid which spans the complete width of the playing field.
public class PyramidKarel extends Karel {

	public void run() {
		//wir vermessen, wie breit das Feld ist (start bei Index 1!)
		int anz_felder = 1;
		//wir berechnen, wie viele Ebenen das Spiel haben muss
		int ebene = 0;
		//wir berechnen, das die maximale Ebene ist, um einen Stopp-Punkt zu erreichen
		int maxEbene;

		//unterste Ebene: hier werden überall Beepers verteilt -> while frontClear
		while (frontIsClear()) {
			//erst den Beeper setzen
			putBeeper();
			//dann die Anzahl der Felder um 1 erhöhen, da wir einen Beeper abgelegt haben
			anz_felder++;
			//ein Feld weitergehen
			move();
		}

		//Geschummelt: in der untersten Ebene gibt es den Sonderfall, dass man geblockt ist und dennoch ablegen muss
		putBeeper();

		//Print ist zum Debug super! Gerade bei Grafikanwendungen
		System.out.println("Anz. Felder: " + anz_felder);

		//maximale Ebene errechnen: im Bsp. 11 breit und 6 hoch (12/2 = 6)
		if (anz_felder % 2 == 1) {
			maxEbene = (anz_felder + 1)/2;
		} else {
			maxEbene = anz_felder/2;
		}

		System.out.println("Max Ebene: " + maxEbene);

		//das Programm läuft nur so lange, bis die oberste Ebene erreicht ist
		while (ebene < maxEbene) {

			//zum Abschluss der Reihe wichtig, nachdem alle Beeper platziert wurden
			while (frontIsClear()) {
				move();
			}

			//blockiert und:....
			//nach Osten oder
			if (frontIsBlocked() && facingEast()) {
				System.out.println("Facing east");
				turnLeft();
				move();
				turnLeft();
			//nach Westen
			} else if (frontIsBlocked() && facingWest()) {
				System.out.println("Facing west");
				turnRight();
				move();
				turnRight();
			}

			//die nächste Ebene ist erreicht
			ebene++;
			System.out.println("New leven: " + ebene);
			//wir müssen jetzt abhängig von der Ebene Felder laufen
			moveTimes(ebene);
			//außerdem abhängig: die Anzahl der Beeper, die wir ablegen müssen
			int beepersToPut = anz_felder - (2*ebene);
			System.out.println("beepersToPut: " + beepersToPut);
			//die passende Anzahl an Beepern ablegen und pro Beeper 1 Feld bewegen
			putBeeperAndMove(beepersToPut);
		}
		
	}

	//Rechtsdrehung
	public void turnRight() {
		turnLeft();
		turnLeft();
		turnLeft();
	}

	//n Schritte gehen
	//wird genutzt, um abhängig von der Ebene Schritte zu laufen
	//in der zweiten Ebene -> 1 freies Feld, bevor ein Beeper gelegt werden soll
	public void moveTimes(int n) {
		System.out.println("Move " + n + " times");
		//wenn wir 4 übergeben: 4 -> 3 -> 2 -> 1 ! -> 0 geht nicht, da nicht > 0
		while (n > 0) {
			move();
			n--;
		}
	}

	//ähnlich wie moveTimes
	//zusätzlich werden noch Beeper abgelegt
	public void putBeeperAndMove(int n) {
		while (n > 0) {
			putBeeper();
			move();
			n--;
		}
	}
}
