import stanford.karel.*;

// Goal: Place beepers in a checkerboard pattern across the complete playing field.
public class CheckerboardKarel extends Karel {

	public void run() {
		boolean rowIsEven = false;

		turnLeft();

		while (!(facingEast() && frontIsBlocked())) {
			if (!rowIsEven) {
				putBeepersOdd();
				rowIsEven = true;
				backToStart();
			}
			nextColumn();
			if (rowIsEven) {
				putBeepersEven();
				rowIsEven = false;
				backToStart();
			}
			nextColumn();
		}
		System.out.println("Done");
	}

	public void putBeepersOdd() {
		while (frontIsClear()) {
			putBeeper();
			move();
			if (frontIsClear()) {
				move();
			} else {
				break;
			}
		}
	}

	public void putBeepersEven() {
		while (frontIsClear()) {
			move();
			putBeeper();
			if (frontIsClear()) {
				move();
			} else {
				break;
			}
		}
	}

	public void turnAround() {
		turnLeft();
		turnLeft();
	}

	public void nextColumn() {
		move();
		turnLeft();
	}

	public void backToStart() {
		turnAround();
		while (frontIsClear()) {
			move();
		}
		turnLeft();
	}
}
