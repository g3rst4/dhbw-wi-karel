import stanford.karel.*;

// Goal: Create a Fibonacci sequence of beepers until the right border of the playing field.
public class FibonacciKarel extends Karel {

	public void run() {
		int n = 10;
		for(int i = 3; i <= n; i ++) {
			putNBeepers(fibonacciRecursiv(i));
			move();
		}
	}

	public int fibonacciRecursiv(int n) {
		if ( n <= 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		} else {
			return (fibonacciRecursiv(n-2) + fibonacciRecursiv(n-1));
		}
	}

	public void fibonacciInterativ(int n) {
		int a = 1;
		int b = 1;
		int h;

		for(int i = 0; i < n; i++) {
			putNBeepers(a+b);
			h = b;
			b = a + b;
			a = h;
			move();
		}
	}

	public void putNBeepers(int n) {
		System.out.println(n);
		while (n > 0) {
			putBeeper();
			n--;
		}
	}


	
}
