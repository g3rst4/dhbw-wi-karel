import stanford.karel.*;

// Goal: Jump each hurdle individually (only as high as necessary) and reach the bottom-right corner.
public class HurdleJumpingKarel extends Karel {

	public void run() {
		while (true) {
			nextHurdle();
			climbUp();
			turnAtPeek();
			backToGround();
		}
	}

	public void climbUp() {
		turnLeft();
		while (frontIsClear() && rightIsBlocked()) {
			move();
		}
	}

	public void turnAtPeek() {
		turnRight();
		move();
		turnRight();
	}

	private void turnRight() {
		turnLeft();
		turnLeft();
		turnLeft();
	}

	public void backToGround() {
		while (frontIsClear()) {
			move();
		}
		turnLeft();
	}

	public void nextHurdle() {
		while (frontIsClear()) {
			move();
		}
	}
}
