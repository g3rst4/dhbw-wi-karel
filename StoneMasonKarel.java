import stanford.karel.*;

// Goal: Restore the columns by placing beepers in the appropriate spots.
public class StoneMasonKarel extends Karel {

	//run ruft die beiden Teile der Problemlösung auf
	public void run() {
		//wie viele Säulen sollen gebaut werden
		int numberOfColumnsToMake = 4;
		while (numberOfColumnsToMake > 0) {
			makeColumn();
			findNextColumn();
			numberOfColumnsToMake--;
		}
	}

	//diese Methode baut die Säulen
	//geht immer von der gleichen Ausgangssituation aus
	//liegt kein Beeper? -> Beeper setzen -> für jedes Feld
	private void makeColumn() {
		//ausrichten: nach oben schauen
		turnLeft();
		//solange keine Mauer
		while (frontIsClear()) {
			//wenn noch kein Beeper liegt -> einen legen
			if (!beepersPresent()) {
				putBeeper();
			}
			//in jedem Fall laufen
			move();
		}
		//fängt den Fall ab, dass wir ganz oben sind und die Wand jetzt geblockt wäre
		if (!beepersPresent()) {
			putBeeper();
		}
		//ganz oben drehen
		turnOnEnd();
		//wieder runterlaufen
		while (frontIsClear()) {
			move();
		}
		//wieder ausrichten, damit wir zur nächsten Säule laufen können
		turnLeft();
	}

	//Wo ist die nächste Säule?
	//hier hard-gecoded -> 4 Felder Abstand
	private void findNextColumn() {
		int steps = 4;

		//4 Schritte machen
		while (steps > 0) {
			move();
			steps--;
		}
	}

	//lediglich eine Vereinfachung, nicht wirklich nötig
	private void turnOnEnd() {
		turnLeft();
		turnLeft();
	}
	
}
